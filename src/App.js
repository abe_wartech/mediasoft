import React, { Component } from "react";
import { BackTop, Typography, Row, Col } from "antd";
import AOS from "aos";

const logomediasoft = require("./assets/logo-mediasoft.svg");
const ig = require("./assets/ig-icon.svg");
const li = require("./assets/li-icon.svg");
const cb = require("./assets/cb-icon.svg");
const invo = require("./assets/invo-button.svg");
const fin = require("./assets/finternet-button.svg");
const gb = require("./assets/general-button.svg");
const arrow = require("./assets/arrow-icon.svg");
const logomediasoftmobile = require("./assets/logo-mediasoft-mobile.svg");
const igmobile = require("./assets/ig-icon-mobile.svg");
const limobile = require("./assets/li-icon-mobile.svg");
const cbmobile = require("./assets/cb-icon-mobile.svg");
const invomobile = require("./assets/invo-button-mobile.svg");
const finmobile = require("./assets/finternet-button-mobile.svg");
const gbmobile = require("./assets/general-button-mobile.svg");

class App extends Component {
  state = {
    width: window.innerWidth,
  };

  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50,
    });
  };

  UNSAFE_componentWillMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  render() {
    const { width } = this.state;
    const isMobile = width <= 500;
    //untuk pc//
    if (!isMobile) {
      return (
        <div>
          <Row className="section1" align="middle" justify="space-between">
            <Col md={12} lg={12} xl={12} style={{ marginTop: "-4rem" }}>
              <a href="/">
                <img src={logomediasoft} />
              </a>
            </Col>
            <Col
              md={12}
              lg={12}
              xl={12}
              align="right"
              style={{ marginTop: "-4rem" }}
            >
              <a href="/">
                <img
                  src={ig}
                  style={{ paddingLeft: "1rem", paddingRight: "1rem" }}
                />
              </a>
              <a href="/">
                <img
                  src={li}
                  style={{ paddingLeft: "1rem", paddingRight: "1rem" }}
                />
              </a>
              <a href="/">
                <img
                  src={cb}
                  style={{ paddingLeft: "1rem", paddingRight: "1rem" }}
                />
              </a>
            </Col>
            <Col md={24} lg={24} xl={24}>
              <Typography className="main">
                Better Life Through Internet
                <br />
                and Digital Media
              </Typography>
            </Col>
            <Col md={24} lg={24} xl={24}>
              <Typography className="typograph-main">About</Typography>
            </Col>
            <Col md={24} lg={24} xl={24}>
              <Typography className="typograph-sub">
                Mediasoft is a digital media and internet holding company based
                in Indonesia. Since
                <br />
                its establishment in 2016 as Oxdream Group, we have established
                and invested digital
                <br />
                based media and internet companies in Indonesia.{" "}
              </Typography>
            </Col>
          </Row>
          <Row className="logo-row" justify="space-between">
            <Col md={8} lg={8} xl={8}>
              <div className="container">
                <a href="https://invo.co.id/">
                  <img src={invo} className="logo" />
                  <div className="overlay">
                    <span style={{ paddingRight: "0.6rem" }}>Visit Link</span>
                    <img src={arrow} />
                  </div>
                </a>
              </div>
            </Col>
            <Col md={8} lg={8} xl={8}>
              <div className="container">
                <a href="https://finternet.co.id/">
                  <img src={fin} className="logo" />
                  <div className="overlay">
                    <span style={{ paddingRight: "0.6rem" }}>Visit Link</span>
                    <img src={arrow} />
                  </div>
                </a>
              </div>
            </Col>
            <Col md={8} lg={8} xl={8}>
              <div className="container">
                <a href="https://generaltech.co.id/">
                  <img src={gb} className="logo" />
                  <div className="overlay">
                    <span style={{ paddingRight: "0.6rem" }}>Visit Link</span>
                    <img src={arrow} />
                  </div>
                </a>
              </div>
            </Col>
          </Row>
          <Row style={{ marginTop: "6rem" }}>
            <Col
              md={24}
              lg={24}
              xl={24}
              style={{ backgroundColor: "#F3F3F3", height: "7vh" }}
            >
              <Typography
                style={{
                  textAlign: "center",
                  paddingTop: "0.8rem",
                  fontWeight: "bold",
                }}
              >
                Copyright @ 2020 Mediasoft
              </Typography>
            </Col>
          </Row>
          <BackTop visibilityHeight={450} />
        </div>
      );
    } else {
      //untuk mobile//
      return (
        <div>
          <Row
            className="section1-mobile"
            type="flex"
            justify="center"
            align="middle"
          >
            <Col xs={12} style={{ marginTop: "-2rem" }}>
              <a href="/">
                <img src={logomediasoftmobile} />
              </a>
            </Col>
            <Col xs={12} align="right" style={{ marginTop: "-2rem" }}>
              <a href="/">
                <img
                  src={igmobile}
                  style={{ paddingLeft: "0.5rem", paddingRight: "0.5rem" }}
                />
              </a>
              <a href="/">
                <img
                  src={limobile}
                  style={{ paddingLeft: "0.5rem", paddingRight: "0.5rem" }}
                />
              </a>
              <a href="/">
                <img
                  src={cbmobile}
                  style={{ paddingLeft: "0.5rem", paddingRight: "0.5rem" }}
                />
              </a>
            </Col>
            <Col xs={24}>
              <Typography
                style={{
                  fontSize: "8vw",
                  fontWeight: "bold",
                  marginTop: "-4.5rem",
                  paddingLeft: "1rem",
                }}
              >
                Better Life Through Internet and Digital Media
              </Typography>
            </Col>
            <Col xs={24}>
              <Typography
                style={{
                  fontSize: "3vw",
                  fontWeight: "bold",
                  paddingLeft: "1rem",
                  marginTop: "-4rem",
                  color: "#2D41C5",
                }}
              >
                About
              </Typography>
            </Col>
            <Col xs={24}>
              <Typography
                style={{
                  fontSize: "3vw",
                  marginTop: "-8.5rem",
                  paddingLeft: "1rem",
                  paddingRight: "1rem",
                }}
              >
                Mediasoft is a digital media and internet holding company based
                in Indonesia. Since
                <br />
                its establishment in 2016 as Oxdream Group, we have established
                and invested digital
                <br />
                based media and internet companies in Indonesia.{" "}
              </Typography>
            </Col>
          </Row>
          <Row className="section-button" justify="space-between">
            <Col xs={24} style={{ marginBottom: "2rem" }}>
              <div className="container-mobile">
                <a href="https://invo.co.id/">
                  <img src={invomobile} className="brand-mobile" />
                  <div className="overlay-mobile">
                    <span style={{ paddingRight: "0.6rem" }}>Visit Link</span>
                    <img src={arrow} />
                  </div>
                </a>
              </div>
            </Col>
            <Col xs={24} style={{ marginBottom: "2rem" }}>
              <div className="container-mobile">
                <a href="https://finternet.co.id/">
                  <img src={finmobile} className="brand-mobile" />
                  <div className="overlay-mobile">
                    <span style={{ paddingRight: "0.6rem" }}>Visit Link</span>
                    <img src={arrow} />
                  </div>
                </a>
              </div>
            </Col>
            <Col xs={24} style={{ marginBottom: "2rem" }}>
              <div className="container-mobile">
                <a href="https://generaltech.co.id/">
                  <img src={gbmobile} className="brand-mobile" />
                  <div className="overlay-mobile">
                    <span style={{ paddingRight: "0.6rem" }}>Visit Link</span>
                    <img src={arrow} />
                  </div>
                </a>
              </div>
            </Col>
          </Row>
          <Row style={{ marginTop: "6rem" }}>
            <Col xs={24} style={{ backgroundColor: "#F3F3F3", height: "7vh" }}>
              <Typography
                style={{
                  textAlign: "center",
                  paddingTop: "0.8rem",
                  fontWeight: "bold",
                }}
              >
                Copyright @ 2020 Mediasoft
              </Typography>
            </Col>
          </Row>
          <BackTop visibilityHeight={450} />
        </div>
      );
    }
  }
}

export default App;
